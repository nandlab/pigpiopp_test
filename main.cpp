#include <iostream>
#include "pigpiopp.h"

#include <unistd.h>

using namespace std;

extern "C" void gpio_cb(int pi, unsigned user_gpio, unsigned level, uint32_t tick) {
    (void) pi;
    (void) user_gpio;
    (void) tick;
    cout << (level ? "high" : "low") << endl;
}

int main()
{
    const int PIN = 7;
    pigpiopp::client pi(nullptr, nullptr);
    pi.set_glitch_filter(PIN, 10000);
    pi.callback(PIN, pigpiopp::EITHER_EDGE, gpio_cb);
    getchar();
    cout << "Good bye!" << endl;
    return 0;
}
